package com.techotom

class Lib {
  static def getMsg(fragment) {
    return "Hello, ${fragment}"
  }

  static def printMsg(fragment) {
    println getMsg(fragment)
  }

  static def getOptions(args) {
    def cli = new CliBuilder(usage:'TODO') // TODO get app name
    cli.source(args:1, argName:'file', 'some file')
    def options = cli.parse(args)
    if (!validate(options)) {
      println cli.usage()
      return false
    }
    return options
  }

  static def validate(options) {
    return options.source
  }
}

