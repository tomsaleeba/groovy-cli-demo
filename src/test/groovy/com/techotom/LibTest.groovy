import spock.lang.Specification
import com.techotom.Lib

class LibTest extends Specification {
    def "getMsg01: can build a greeting"() {
        setup:
        def objectUnderTest = new Lib()

        when:
        def result = objectUnderTest.getMsg('blah')

        then:
        result == 'Hello, blah'
    }
}
