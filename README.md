> a prototype of a Groovy command line app, built using Gradle

At this stage, it's just an app that parses the command line arguments, demonstrates some project structure (importing Lib.groovy) and has a simple unit test.

## Building
 1. build the project with
    ```bash
      ./gradlew uberJar
    ```

## Running the project
You have a few options for running the project
 1. you can run the built distributable with
    ```bash
      java -jar build/libs/groovy-cli-demo.jar --source blah # or
      java -jar build/libs/groovy-cli-demo.jar -source blah
    ```
 1. you can run the raw project via Gradle with
    ```bash
      ./gradlew run # can't pass args to app though :(
    ```
 1. you can run the raw project directly via Groovy with
    ```bash
      ./quick-run.sh -source blah
    ```
